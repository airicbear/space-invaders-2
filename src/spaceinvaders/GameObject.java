/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import javafx.scene.image.Image;

/**
 *
 * @author zeus
 */
class GameObject {
    protected int[] position, scale;
    protected Image image;
    
    public GameObject(int[] pos, int[] scl, Image img) {
        position = pos;
        scale = scl;
        image = img;
    }
    
    /**
     * @return the position of the GameObject
     */
    public int[] pos() {
        return position;
    }
    
    /**
     * @return the scale of the GameObject
     */
    public int[] scl() {
        return scale;
    }
    
    /**
     * @return the image of the GameObject
     */
    public Image img() {
        return image;
    }
    
    /**
     * @param other
     * @return true if this is colliding with other
     */
    public boolean colliding(GameObject other) {
        return this.position[0] >= other.position[0]
        && this.position[0] <= other.position[0] + other.scale[0]
        && this.position[1] >= other.position[1]
        && this.position[1] <= other.position[1] + other.scale[1];
    }
    
    public boolean colliding(GameObject[] group) {
        for(GameObject obj : group) {
            if(colliding(obj)) {
                return true;
            }
        }
        return false;
    }
}
