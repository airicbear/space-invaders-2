/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import javafx.scene.image.Image;

/**
 *
 * @author zeus
 */
public abstract class GamePiece extends Movable {
    
    protected Movable projectile;
    
    public GamePiece(int[] pos, int[] scl, Image img) {
        super(pos, scl, img);
        projectile = new Movable(new int[] {pos[0] + scl[0] / 2, pos[1]}, new int[] {10, 10}, new Image("file:shield.png"));
        projectile.speed = 10;
    }
    
    public abstract void die();
    
    public abstract void shoot();
    
    public Movable getProjectile() {
        return projectile;
    }
    
}
