/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author zeus
 */
public class Main extends Application {
    
    private static final int WIDTH = 200, HEIGHT = 100;
    private static final Color BACKGROUND = Color.BLACK;

    @Override
    public void start(Stage primaryStage) {
        
        File file = new File("./highscore.txt");
        
        if(!file.exists()) try {
            file.createNewFile();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
                     
        StackPane root = new StackPane();
                
        Scene scene = new Scene(root, WIDTH, HEIGHT);        
        
        Button btn = new Button();
        btn.setText("PLAY");
        btn.setOnAction((ActionEvent event) -> {
            (new SpaceInvaders()).start();
            primaryStage.close();
        });
        
        
        root.getChildren().add(btn);
        primaryStage.setOnCloseRequest(req -> System.exit(0));
        primaryStage.setTitle("Space Invaders");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
