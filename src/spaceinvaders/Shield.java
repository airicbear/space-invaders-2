/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import javafx.scene.image.Image;

/**
 *
 * @author zeus
 */
public class Shield extends GameObject {
    
    private int hp;
    
    public Shield(int[] pos) {
        super(pos, new int[]{100, 50}, new Image("file:shield.png"));
        hp = 1;
    }
    
}
