/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import javafx.animation.AnimationTimer;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author zeus
 */
public class SpaceInvaders {
    
    private static final int WIDTH = 800, HEIGHT = 600;
    private static final Color BACKGROUND = Color.BLACK;
        
    private static final StackPane layout = new StackPane();
    private static final Scene scene = new Scene(layout, WIDTH, HEIGHT);
    private static final Canvas canvas = new Canvas(scene.getWidth(), scene.getHeight());
    private static final GraphicsContext ctx = canvas.getGraphicsContext2D();
    private static final Graphics gfx = new Graphics(ctx);

    public void start() {
        // Setup
        Stage stage = new Stage();
        stage.setTitle("Space Invaders");
        stage.setOnCloseRequest(req -> System.exit(0)); 
        Ship ship = new Ship(new int[] {WIDTH / 2, HEIGHT - HEIGHT / 10}, new int[] {55, 30});        
        GameObject[] shields = new GameObject[4];
        for(int i = 0; i < shields.length; i++) {
            shields[i] = new Shield(new int[]{i * 200 + 50, HEIGHT - HEIGHT / 4});
        }
        
        gfx.drawBackground(WIDTH, HEIGHT, BACKGROUND);
        
        // Ship controls
        scene.setOnKeyPressed(e -> {
            switch(e.getCode()) {
                // Move left
                case LEFT:
                case A:
                    ship.setVelocity(-1);
                    break;
                    
                // Move right
                case RIGHT:
                case D:
                    ship.setVelocity(1);
                    break;
                
                // Shoot
                case SPACE:
                    ship.shoot();
                    break;
                    
                default:
                    break;
            }
        });

        scene.setOnKeyReleased(e -> {
            switch(e.getCode()) {
                
                // Stop moving
                case LEFT:
                case RIGHT:
                case A:
                case D:
                    ship.setVelocity(0);
                    break;
                    
                default:
                    break;
            }
        });
        stage.setScene(scene);
        
        layout.getChildren().add(canvas);
        
        // Threads
        Runnable move_ship = () -> {
            if(inBounds(ship)) {
                ship.move();
            }
        };
        
        Runnable move_ship_projectile = () -> {
            if(ship.getProjectile().position[1] > 0 && !ship.getProjectile().colliding(shields)) {
                ship.getProjectile().move();
            } else {
                ship.resetProjectile();
            }
        };
        
        // Game loop
        AnimationTimer loop = new AnimationTimer() {
            
            @Override
            public void handle(long now) {
                
                // Update
                (new Thread(move_ship)).start();
                (new Thread(move_ship_projectile)).start();
                
                // Draw background
                gfx.drawBackground(WIDTH, HEIGHT, BACKGROUND);
                
                // Draw shields
                for(GameObject obj : shields) {
                    gfx.drawGameObject(obj);
                }
                
                // Draw ship
                gfx.drawGameObject(ship);
                
                if(ship.getProjectile().velocity[1] != 0) {
                    gfx.drawGameObject(ship.getProjectile());
                }
                
                // Draw ship values
                ctx.setFill(Color.WHITE);
                ctx.fillText("SCORE " + ship.getScore(), WIDTH / 25, HEIGHT / 20);
                ctx.fillText("LIVES " + ship.getLives(), WIDTH - WIDTH / 10, HEIGHT / 20);
            }
            
        };
        loop.start();
        
        stage.show();
    }
    
    private boolean inBounds(Movable obj) {
        return (obj.pos()[0] > 0 && obj.vel()[0] < 0)
            || (obj.pos()[0] + obj.scl()[0] < WIDTH && obj.vel()[0] > 0);
    }
    
}